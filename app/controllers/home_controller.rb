class HomeController < ApplicationController
  def index
  end

  def search
    render json: Yelp.client.search('89148', {
      category_filter: 'restaurants',
      term: params[:term]
    }).businesses.sample(8)
  end
end
